package DOMJUDGE;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BANTest {

	@Test
	public void testEasy() {
		String lin1 = "Tengo un gran sacapuntas OwO";
		String lin2 = "Es hada UwU";
		String lin3 = "Muerte a azul, -3 acuacolas";
		String lin4 = "Adolfito x4 de hada";
		String lin5 = "Nada útil escrito";
		assertTrue(BAN.eeehhhh(lin1));
		assertTrue(BAN.eeehhhh(lin2));
		assertFalse(BAN.eeehhhh(lin3));
		assertTrue(BAN.eeehhhh(lin4));
		assertFalse(BAN.eeehhhh(lin5));
	}

	@Test
	public void testCabron() {
		String lin1 = "Es adah 53 2more owu";
		assertFalse(BAN.eeehhhh(lin1));
		String lin2 = "ovo ovu adolfíto";
		assertFalse(BAN.eeehhhh(lin2));
		String lin3 = "eshada 345 pen";
		assertFalse(BAN.eeehhhh(lin3));
		String lin4 = "hada_owo_uwu 35";
		assertTrue(BAN.eeehhhh(lin4));

	}

}
